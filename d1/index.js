// SECTION - Objects

/* 
    - objects are data types that are used to represent real world objects
    - collection of related data and/or functionalities
    - in JavaScript, most features like strings and arrays are considered to be objects

    - their difference in JavaScript objects is that the JS object has "key: value" pair
    - keys are also referred to as "properties" while the value is the figure on the right side of the colon
    - objects are commonly initialized or declared using the let + objectName and this object value will start
    with curly brace ({}) that is also called "Object Literals"

    SYNTAX (using initializers):
        let/const objectName {
            keyA: valueA,
            keyB: valueB,
            keyC: valueC
        }
*/

let cellphone = {
    name: "Nokia 3210",
    manufacturedDate: 1999
}
console.log("Result from creating objects using initializers");
console.log(cellphone);
console.log(typeof cellphone);

/* let car = {
    model: "GT-R R34",
    yearReleased: 1998
}
console.log(car);
console.log(typeof car); */

// creating objects using constructor function
/* 
    - creates a reusable function to create/construct several objects that have same data structure
    - this is useful for creating multiple instances/copies of an object
    - instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it

    SYNTAX:
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB
        }
*/
/* 
    - this keyword allows us to assign a new property for the object by associating them with values received from a constructor function's parameters
*/
function Laptop(name, manufacturedDate) {
    this.name = name;
    this.manufacturedDate = manufacturedDate;
}
// this is a unique instance/copy of the Laptop object
/* 
    - the "new" keyword creates an instance of an object
    - objects and instances are often interchanged because object literals and instances are distinct/unique
*/
let laptop = new Laptop ("Dell", 2012);
console.log("Result from creating objects using consctructor function:");
console.log(laptop);
console.log(typeof laptop);
// creates a new Laptop object
let laptop2 = new Laptop ("Lenovo", 2008);
console.log(laptop2);
console.log(typeof laptop2);

let computer = {}
let myComputer = new Object();
console.log(computer);
console.log(typeof computer);
console.log(myComputer);
console.log(typeof myComputer);

// Accessing Objects
/* 
    - use dot notation to access the property/key
    SYNTAX:
        objectName.property

*/

// using dot notation
console.log(`Result from dot notation: ${laptop.name}`);
console.log(`Result from dot notation: ${laptop.manufacturedDate}`);

//array of objects
let laptops = [laptop, laptop2];
// to get the properties of objects inside an array, you can do: array[index].property
console.log(laptops[1].name);
console.log(laptops[0].manufacturedDate);
// using string inside the second square bracket would let us access the property inside the objects, but this is not recommended
console.log(laptops[0]["manufacturedDate"]);

// using square brackets
// console.log(`Result from dot notation ${laptop[name]}`); returns undefined because there is no element inside the laptop that can be accessed using the square bracket notation

// SECTION - initializing, adding, deleting, and reassigning object properties
/* 
    - like any other variables in javascript, objects may have their properties initialized or added after the object was created/declared
    - this is useful for times when an objec's properties are undetermined at the time of creating them
*/
let car = {};
console.log(car);
car.name = "Honda Vios";
car.manufacturedDate = 2022;
console.log(car)

/* 
    - while using square brackets will give the same feature as using dot notation, it might lean us to create unconventional naming for the properties which might lead future confusions when we try to access them
*/
car["manufactured date"] = 2019;
console.log(car);

// deleting of properties
/* 
    - to delete object properties, use: delete object.property || delete object["propertyName"] (not recommended)
*/
delete car["manufactured date"];
//delete car.manufacturedDate;
console.log(car)

// reassigning of properties
/* 
    - to reassign property values, use: object.property = newValue;
*/
car.name = "Dodge Charger R/T";
console.log(car);

// Object Methods
/* 
    - an object method is a function that is set by the dev to be the value of one of the properties
    - they are also functions and one of the differences they have is that methods are functions related to a specific object
    - these are useful for creating object-specific functions which are used to perform tasks on them
*/

//when trying to use an outside function to be stored to an object as a property method, it would return an error
/* 
    function personTalk(){
        console.log(`Hello! My name is ${this.name}`);
    }
    let person = {
        name: "John",
        talk: personTalk()
    }
*/

let person = {
    name: "John",
    talk: function(){
        console.log(`Hello! My name is ${this.name}`);
    },
    // walk: function() {
    //     console.log(`${this.name} walked 25 steps`);
    // }
}

//can also add an object method with the code below
person.walk = function() {
    console.log(`${this.name} walked 25 steps`);
};
console.log(person);
console.log(`Result from object methods:`);
person.talk();
person.walk();

// nested object
let friend = {
    firstName: "Joe",
    lastName: "Smith",
    address: {
        city: "Austin",
        state: "Texas"
    },
    emails: ['joe@mail.com', "johnHandsome@mail.com"],
    introduce: function(){
        console.log(`Hello! My name is ${this.firstName} ${this.lastName}`);
    }
}

friend.introduce();
console.log(friend);
// accessing a nested object property
console.log(friend.address.city);

/* 
    Scenario
        - we would like to create a game that would have several pokemon interact with each other
        - every pokemon would have the same stats, properties, and functions
*/

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    /* tackle: function() {
        console.log(`${this.name} tackled ${targetPokemon}`);
        console.log(`${targetPokemon}'s health is now reduced to ${_targetPokemonHealth_}`);
    }, */
    faint: function() {
        console.log("Pokemon fainted.");
    }
}
console.log(myPokemon);

// using constructor function
function Pokemon(name, level) {
    //properties
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;
    //methods
    this.tackle = function(target) {
        console.log(`${this.name} tackled ${target.name}`);
        target.health = target.health - this.attack;
        console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`);
        if (target.health === 0) {
            target.faint()
        }
    },
    this.faint = function() {
        console.log(`${this.name} fainted.`);
    }
}

//creating new pokemon

let bulbasaur = new Pokemon("Bulbasaur", 8);
let onyx = new Pokemon("Onyx", 4);

// using the tackle method
bulbasaur.tackle(onyx);
