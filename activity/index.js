// added new variables, will be useful to the added new methods :)
let wildPokemons = ["Bulbasaur", "Ivysaur", "Venusaur", "Charmander", "Charmeleon", "Charizard", "Squirtle", "Wartortle", "Blastoise", "Caterpie", "Metapod", "Butterfree", "Weedle", "Kakuna", "Beedrill", "Pidgey", "Pidgeotto", "Pidgeot", "Rattata", "Raticate", "Spearow", "Fearow", "Ekans", "Arbok", "Pikachu", "Raichu", "Sandshrew", "Sandslash", "Nidoran♀", "Nidorina", "Nidoqueen", "Nidoran♂", "Nidorino", "Nidoking", "Clefairy", "Clefable", "Vulpix", "Ninetales", "Jigglypuff", "Wigglytuff", "Zubat", "Golbat", "Oddish", "Gloom", "Vileplume", "Paras", "Parasect", "Venonat", "Venomoth", "Diglett", "Dugtrio", "Meowth", "Persian", "Psyduck", "Golduck", "Mankey", "Primeape", "Growlithe", "Arcanine", "Poliwag", "Poliwhirl"];
let newPokemon = "";

//List of New Properties/Methods added that are not specified in activity instructions (stretch goals :D)
/*  
    Properties:
    .nextPokemon = empty object that will be filled with a new pokemon object when .choose(index) is used

    Trainer Methods:
    .choose() = lets trainer to choose a new pokemon when the first chosen pokemon faints
    .walk() = trainer will 'walk' and log steps (randomized). Has a 40% chance of encountering a wild Pokemon
    .catch() = lets trainer to catch the wild pokemon that appeared. Has 30% chance of success
    .release() = lets trainer to release a pokemon from his roster. Accepts pokemon name as argument instead of index. Not case sensitive

    Pokemon Methods
    .specialAtk() = a new attack that multiplies a pokemon's level to its current attack value
    **suggested to test these methods in the console for better game immersion
*/

let trainer = {
    name: "Dom Toretto",
    age: 22,
    pokemon: ["Onyx", "Rapidash", "Geodude", "Exeggutor"],
    //added new property
    nextPokemon: {},
    friends: {
        america: ["Brian", "Letty", "Jakob"],
        philippines: ["Cardo", "Pacman", "Efren"]
    },
    // allow trainer to specify pokemon using index of pokemon array
    talk: function(index) {
        //check if index is valid
        if (typeof index === 'number' && index < this.pokemon.length) {
            console.log(`${this.pokemon[index]}! I choose you!`);
        } else {
            console.log(`This pokemon does not exist!`);
        }
    },
    choose: function(index) {      
        //check if index is valid
        if (typeof index === 'number' && index < this.pokemon.length) {
            /* create a new Pokemon object with specified name, and level equals to the length of pokemon name, and assign object to nextPokemon(nested object) */
            this.nextPokemon = new Pokemon(this.pokemon[index], this.pokemon[index].length);
            this.talk(index);
        } else {
            console.log(`This pokemon does not exist!`);
        }
    },
    walk: function() {
        randomStep = Math.floor(Math.random() * 20 + 1);
        console.log(`${this.name} walked ${randomStep} ${(randomStep > 1) ? 'steps' : 'step'}`);
        // show wild pokemons with 40% probability
        if (Math.random() < 0.4) {
            // randomize which pokemon will appear
            newPokemon = wildPokemons[Math.floor(Math.random()*wildPokemons.length)];
            console.log(`A wild ${newPokemon} appeared! Type trainer.catch() to catch it!`);
        }
    },
    catch: function() {
        // 30% chance of catching wild pokemon
        const randomChance = Math.random() < 0.3;
        if (newPokemon !== "") {
            if (randomChance) {
                // add to trainer pokemon array
                this.pokemon.push(newPokemon);
                console.log(`You caught ${newPokemon}! Type trainer.pokemon to check.`)
            } else {
                console.log(`Oops! ${newPokemon} got away!`)
                newPokemon = "";
            }
        } else {
            console.log("No wild pokemon here.")
        }
    },
    release: function(pokemonName) {
        // make first letter uppercase and make following letters lowercase
        pokemonName = pokemonName.charAt(0).toUpperCase() + pokemonName.slice(1).toLowerCase();
        let index = this.pokemon.indexOf(pokemonName);
        let releasedPokemon = this.pokemon[index];
        if (index === -1 || typeof pokemonName === 'undefined') {
            console.warn("You do not own this pokemon.");
        } else {
            this.pokemon.splice(index, 1);
            return `You have released ${releasedPokemon}.`;
        }
    }
}
console.log(trainer);

// using dot and square bracket notation
console.log(`Result of dot notation: \n${trainer.name}`);
console.log(`Result of square bracket notation:`);
console.log(trainer["pokemon"]);

// using the talk method
console.log(`Result of talk method:`)
trainer.talk(1);

// create pokemon object using constructor function
function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = 1.5*level;
    this.tackle = function(target){
        // added a condition where a fainted pokemon cannot launch/receive a tackle
        if (this.health <= 0 || target.health <= 0) {
            console.warn(`A pokemon cannot launch/receive a tackle if it's fainted. Choose another.`);
        } else {
            target.health = target.health - this.attack; 
            console.log(`${this.name} tackled ${target.name}`);
            console.log(`${target.name}'s health is now reduced to ${target.health}`);
            if(target.health <= 0) {
                target.faint();
            }
        }
    },
    this.specialAtk = function(target) {
        // added a condition where a fainted pokemon cannot launch/receive a tackle
        if (this.health <= 0 || target.health <= 0) {
            console.warn(`A pokemon cannot launch/receive a special attack if it is fainted. Choose another.`);
        } else {
            target.health = target.health - this.level*this.attack; 
            console.log(`${this.name} tackled ${target.name}`);
            console.log(`${target.name}'s health is now reduced to ${target.health}`);
            if(target.health <= 0) {
                target.faint();
            }
        }
    }
    this.faint = function() {
        console.log(`${this.name} fainted.`)
    }
}

// create/instantiate pokemons
let bulbasaur = new Pokemon("Bulbasaur", 10);
let arceus = new Pokemon("Arceus", 100);
// trainer selects Rapidash; .choose(index) is required if you wish to use trainer's pokemon
trainer.choose(1);
// assign Rapidash to a variable; you cannot do {let rapidash = trainer.choose(1)} because the chosen pokemon object will only be assigned to trainer.nextPokemon; thus will return an error because it is undefined
let rapidash = trainer.nextPokemon;
console.log(rapidash);
console.log(bulbasaur)
console.log(arceus)

//testing object methods
bulbasaur.tackle(rapidash)
bulbasaur.specialAtk(rapidash)
bulbasaur.tackle(rapidash)
bulbasaur.specialAtk(rapidash)

arceus.specialAtk(bulbasaur)
arceus.tackle(bulbasaur)
